## HUMM TECH
## Brain.py
## this is where most of the signal processing happens

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
from scipy import signal

# some program constants
alpha_band_Hz = [8.0, 14.0]   # where to look for the alpha peak
beta_band_Hz = [12, 38]   # where to look for the beta peak
noise_band_Hz = [38, 100]  # was 20-40 Hz
NFFT = 400      # pitck the length of the fft
sample_rate = 250.0   # assumed sample rate for the EEG data
f_lim_Hz = [0, 100]      # frequency limits for plotting
t_lim_sec = [10, 120]      # default plot time limits [0,0] will be ignored
alpha_lim_sec = [30, 100]  # default


def Running_Mean_Fast(x, N):
	return np.convolve(x, np.ones((N,))/N)[(N-1):]
# Function to remove any DC Noise, as this will not have been made by the brain
def dcFiltering(dc_Data):
	hp_cutoff_hz = 1.0
	b, a = signal.butter(2,hp_cutoff_hz/(sample_rate/2.0),'highpass')
	dc_removed_data = signal.lfilter(b,a,dc_Data,0)
	return dc_removed_data

#Function for removing the AC Noise from the device, in this case it is 50 and 100 Hz
def acModuleFiltering(ac_data):
	cutoff_hz = np.array([50.0,100.0])#these are the cut off frequencies
	for freq_hz in np.nditer(cutoff_hz):
		bandstop_hz = freq_hz + 3.0 *np.array([-1,1])#setting the band stop
		b, a = signal.butter(3,bandstop_hz/(sample_rate/2.0),'bandstop')
		ac_removed_data = signal.lfilter(b,a,ac_data,0)
	return ac_removed_data


def filtering(data):
	##investigate if this is still needed, indexing may be not useful
	index_data = data[:,[0]] #the first column of the packet is the index of the information
	ch7_uV_data = data[:,[7]]

	no_dc_ch7_uV_data = dcFiltering(ch7_uV_data)
	#repeated for the amount of channels that is used
	no_ac_ch7_uV_data = acModuleFiltering(no_dc_ch7_uV_data)
	return no_ac_ch7_uV_data



def PSDExtraction(data):
	overlap = NFFT - int(0.25 * sample_rate)
	PSD_per_hz, freq, t_spec = mlab.specgram(np.squeeze(data),
								NFFT=NFFT,
								window=mlab.window_hanning,
								Fs=sample_rate,
								noverlap=overlap
								) #Returns the PSD per hertz
	##Converting to per freq Bin
	PSD_per_bin = PSD_per_hz * sample_rate / float(NFFT)
	#delting as it is not required anymore
	del PSD_per_hz

	return PSD_per_bin

def PSDtoRMS(data):
	spectra_PSD = np.mean(data,1)
	#print (spectra_PSD)
	spectra_RMS = np.sqrt(spectra_PSD)


	return spectra_RMS



def butter_bandpass(lowcut, highcut, fs, order):
	nyq = 0.5 *fs
	low = lowcut / nyq
	high = highcut/nyq
	b, a = signal.butter(order,[low , high],btype = 'bandpass')
	return b,a

def AlphaPowerExtraction(data):
	#print(data)
	b,a = butter_bandpass(alpha_band_Hz[0],alpha_band_Hz[1],sample_rate,3)
	y = signal.lfilter(b,a,data)
	#print(y)
	return y


def BetaPowerExtraction(data):	
	b,a = butter_bandpass(beta_band_Hz[0],beta_band_Hz[1],sample_rate,4)
	y = signal.lfilter(b,a,data)
	return y
def InformationExtraction(data):
	overlap = NFFT - int(0.25 * sample_rate)
	data_PSD, freq, t_spec = mlab.specgram(np.squeeze(data),
								NFFT=NFFT,
								window=mlab.window_hanning,
								Fs=sample_rate,
								noverlap=overlap
								) #Returns the PSD per hertz
	PSD_per_bin = data_PSD * sample_rate / float(NFFT)
	return PSD_per_bin, freq, t_spec, overlap

def Alpha_Fuckery(data,freq):
	bool_inds = (freq > alpha_band_Hz[0]) & (freq < alpha_band_Hz[1])
	alpha_max_rms = np.sqrt(np.amax(data[bool_inds,:],0))
	alpha_sum_rms = np.sqrt(np.sum(data[bool_inds,:],0))
	mean_alpha = Running_Mean_Fast(alpha_max_rms,100)
	return mean_alpha
def Beta_Fuckery(data,freq):
	bool_inds = (freq > beta_band_Hz[0]) & (freq < beta_band_Hz[1])
	beta_max_rms = np.sqrt(np.amax(data[bool_inds,:],0))
	mean_beta = Running_Mean_Fast(beta_max_rms,100)
	return mean_beta