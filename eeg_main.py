## HUMM TECH
## eeg_main.py
## main object for EEG to focus algo
## V2 Working with Beta and Alpha Bands being filtered
## Working on interpretation of the data and creating a value
## New Comment
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
from scipy import signal
import bci_io
import Plots
import brain
import sys



def main():
	#file_name = sys.argv[1:]
	#print(file_name)
	data_current = bci_io.read(sys.argv[1:])
	new_current_data = brain.filtering(data_current)
	alpha_data = brain.AlphaPowerExtraction(new_current_data)
	#print(new_current_data)
	#print(alpha_data)
	#Plots.lineGraphPlotTime(new_current_data)
	#Plots.lineGraphPlotTime(alpha_data)
	data_hold,freq,t_spec,overlap = brain.InformationExtraction(new_current_data)
	alpha_hold,alpha_freq,alpha_t,alpha_overlap = brain.InformationExtraction(alpha_data)
	print(alpha_hold)
	data_PSD = brain.PSDExtraction(new_current_data)
	alpha_PSD = brain.PSDExtraction(alpha_data)
	#print(new_current_data[4])
	#print(data_PSD)
	Plots.spectogram(new_current_data)
	#plt.show()
	data_RMS = brain.PSDtoRMS(data_PSD)
	alpha_RMS = brain.PSDtoRMS(alpha_PSD)
	alphaFuckery = brain.Alpha_Fuckery(data_hold,freq)
	beta = brain.Beta_Fuckery(data_hold,freq)
	#print(data_RMS)
	#alpha_RMS = brain.AlphaPowerExtraction(data_RMS)
	beta_RMS = brain.BetaPowerExtraction(data_RMS)
	#Opening a new figure, set sized in inches?
	fig1 = plt.figure(figsize=(7.5,9.25))
	ax1 = plt.subplot(111)
	#creating the time values for the x axis
	t_sec = np.array(range(0,alpha_RMS.size))/250
	alpha_t_sec = np.array(range(0,alpha_data.size))/250
	#Plots.spectogram(alpha_hold)
	plt.plot(t_spec,alphaFuckery)
	plt.ylim(-1,6)
	plt.xlim(10,35)
	plt.ylabel('alpah window uV')
	plt.xlabel('Time')
	fig2 = plt.figure(figsize=(7.5,9.25))
	ax1 = plt.subplot(111)
	#creating the time values for the x axis
	#Plots.spectogram(alpha_hold)
	plt.plot(t_spec,alphaFuckery)
	plt.ylim(0,2)
	plt.xlim(0,90)
	plt.ylabel('Alpha uV')
	plt.xlabel('Time')
	alpha_change = np.gradient(alphaFuckery)
	fig3 = plt.figure(figsize=(7.5,9.25))
	ax1 = plt.subplot(111)
	#creating the time values for the x axis
	#Plots.spectogram(alpha_hold)
	plt.plot(t_spec,alpha_change)
	plt.ylim(-0.05,0.2)
	plt.xlim(10,35)
	plt.ylabel('alpha Change uV')
	plt.xlabel('Time')
	fig4 = plt.figure(figsize=(7.5,9.25))
	ax1 = plt.subplot(111)
	#creating the time values for the x axis
	#Plots.spectogram(alpha_hold)
	plt.plot(t_spec,beta)
	plt.ylim(0,2)
	plt.xlim(0,90)
	plt.ylabel('Beta uV')
	plt.xlabel('Time')
	plt.show()


#this is needed for the end 
if __name__ == '__main__':

	
	main()
