## HUMM TECH
## bci_io.py
## handles all of the communication with the BCI board

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
from scipy import signal


def read(file):
	print('It got into the second file')
	#the file name that is to be read
	fname = file[0]
	#the folder location to save to
	#pname = 'Data/'
	# load data into numpy array
	data = np.loadtxt(fname,delimiter=',',skiprows=5) #Why do we skip row 5??
	return data