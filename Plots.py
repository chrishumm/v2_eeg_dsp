## HUMM TECH
## Plots.py
## Library for the plotting functions

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np
from scipy import signal
## pick the length of the FFT
NFFT = 400
sample_rate = 250

def lineGraphPlotTime(data):
	#Opening a new figure, set sized in inches?
	fig = plt.figure(figsize=(7.5,9.25))
	#ax1 = plt.subplot(111)
	#creating the time values for the x axis
	t_sec = np.array(range(0,data.size))/sample_rate
	plt.plot(t_sec,data)
	plt.ylim(0,1)
	plt.ylabel('EEG (uV')
	plt.xlabel('Time (sec)')
	##Insert a title for the grapgh here
	plt.xlim(t_sec[0],t_sec[-1])

def spectogram(data):

	t_sec = np.array(range(0,data.size))/sample_rate
	overlap = NFFT - int(0.25 * sample_rate)
	PSD_per_hz, freqs, t_spec = mlab.specgram(np.squeeze(data),
								NFFT=NFFT,
								window=mlab.window_hanning,
								Fs=sample_rate,
								noverlap=overlap
								)
	PSD_per_bin = PSD_per_hz * sample_rate / float(NFFT)
	del PSD_per_hz
	#t_spec = t_spec[1:-1:2]
	#data = data[:,1:-1:2]
	fig = plt.figure(figsize=(7.5,9.25))
	#ax1 = plt.subplot(311)
	#creating quater second stewp overlaps
	plt.pcolor(t_spec, freqs, 10*np.log10(PSD_per_bin))
	plt.clim(25-5+np.array([-40,0]))
	plt.xlim(0,90)
	plt.ylim(0,100)
	plt.xlabel('Time (sec)')
	plt.ylabel('Frequency (Hz)')


def plotFreqVsAmp(data):
	fig.plt.figure(figsize = (7.5,9.25))
	axi = subplot(111)
	plt.plot(freq,data,'-')
	plt.xlim(0,100)
	plt.ylim(0,6)##This needs to be looked at for on the fly works
	plt.xlabel('Frequency (Hz)')
	plt.ylabel('RMS Amp')

